const { MongoClient } = require("mongodb");

async function print() {

    const costumers = await getCostumers();

    console.log(costumers);
}

async function getCostumers() {
    const client = new MongoClient("mongodb://localhost:27017");
    try {
        await client.connect();
        const database = client.db("beetrack");
        const collection = database.collection("config");

        return await collection.find().toArray();
    } catch (e) {
        console.log("Error: " + e);
    } finally {
        await client.close();
    }
};

print();